//
//  ViewController.swift
//  NewProject
//
//  Created by iFlair on 08/12/21.
//

import UIKit
import Alamofire
import ObjectMapper
import PromiseKit


// MARK:- VIEWCONTROLLER

class ViewController: UIViewController {
    //https://jsonplaceholder.typicode.com/posts/1
    
    
    // MARK:- OUTLET
    
    @IBOutlet weak var textView: UITextView!
    
    
    
    let url:URL = URL(string: "https://jsonplaceholder.typicode.com/posts/1")!
    var dict1 = [String:AnyObject] ()
    var UserModel:GetModel? = nil
    var userArrayModel = [arrayModel] ()
    var mapModel1 = [MapModel] ()
    
    
    //MARK:- LIFE CYCLE
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textView.text = ""
        
        
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func btnAction_JsonDict(_ sender: UIButton) {
        self.getapiData(url: self.url) { (response) in
            let a = response as? [String:AnyObject]
            print(a?["title"]  as! String)
            
            let newdata = a?["title"] as? String  ?? ""
            print("===========1111111=============")
            self.textView.text = "tittle:- " + newdata
        }
    }
    
    
    @IBAction func btnAction_JsonModel(_ sender: UIButton) {
        self.getapiSigalDataModel(url: self.url) { (response) in
            
            self.UserModel = response
            self.textView.text = self.UserModel?.title
            print(self.UserModel!.body)
            print("===========22222222=============")
            
        }
    }
    
    
    
    @IBAction func btnAction_ArrayModel(_ sender: UIButton) {
        let url1:URL = URL(string: "https://jsonplaceholder.typicode.com/posts")!
                    self.getapiArrayDataModel(url: url1) { (response) in

                    self.userArrayModel = response
                    print("===========333333333=============")
                    print(self.userArrayModel[2].id)
                    self.textView.text = self.userArrayModel[2].title
                 }
        
        getapiObjectMapperDataModel(url: url1).done { (response) in
            self.mapModel1 = response

            print(self.mapModel1[20].id)

        }
    }
    
    
    // MARK:- API DATA IN JSON FORMENT
    
    func getapiData(url:URL , complishHendelar: @escaping (NSDictionary) -> Void){
        AF.request(url,parameters: nil) .responseJSON { (response) in
            switch response.result{
            
            case .success(let data3):
                print(response.result)
                do{
                    let dict = data3 as? [String:AnyObject]
                    self.dict1 = dict ?? [:]
                    complishHendelar(self.dict1 as NSDictionary)
                    print(self.dict1["title"]!)
                }
            case .failure(let err):
                print(err.localizedDescription)
            }
        }
    }
    
    // MARK:- API DATA IN JSON MODEL
    
    func getapiSigalDataModel(url:URL , complishHendelar: @escaping (GetModel) -> Void) -> Void {
        
        AF.request(url,parameters: nil) .response { (response) in
            switch response.result{
            
            case .success:
                
                do{
                    let dataResponse = try JSONDecoder().decode(GetModel.self, from: response.data!)
                    complishHendelar(dataResponse)
                    
                }catch let err{
                    print(err)
                }
                
                
            case .failure(let err):
                print(err.localizedDescription)
            }
        }
    }
    
    // MARK:- API DATA IN JSON Array MODEL
    
    
    func getapiArrayDataModel(url:URL , complishHendelar: @escaping ([arrayModel]) -> Void) -> Void {
        
        AF.request(url,parameters: nil) .response { (response) in
            switch response.result{
            
            case .success:
                
                do{
                    let dataResponse = try? JSONDecoder().decode([arrayModel].self, from: response.data!)
                    complishHendelar(dataResponse!)
                    print("===========okkkkkk=============")
                }catch let err{
                    print(err)
                }
                
                
            case .failure(let err):
                print(err.localizedDescription)
            }
        }
    }
    
    //MARK:- AlamoFire Object Mapper
    func getapiObjectMapperDataModel(url:URL) -> Promise<[MapModel]>  {
        
        return Promise<[MapModel]> { done in
        AF.request(url).responseJSON{ response in
            switch response.result{
            case .success(let data):
                do{
                    print(data)
                    let data1 = Mapper<MapModel>().mapArray(JSONObject: data)
                    done.fulfill(data1!)
                }catch(let err){
                    print(err)
                    done.reject(err)
                }
            case .failure(let error):
                print(error)
                
            }
            
        }
            
        }
    }
    
    func getNewapiResponse(url:String) -> Promise<[MapModel]> {
        
        return Promise<[MapModel]> { done in
            
            AF.request(url).response{ (response) in
                switch response.result{
                
                case .success(let data):
                    let data = Mapper<MapModel>().mapArray(JSONObject: data)
                    done.fulfill(data!)
                case .failure(let err):
                    done.reject(err)
                    
                }
                
            }
            
        }
    }
}

