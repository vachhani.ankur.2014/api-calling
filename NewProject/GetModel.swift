//
//  RootClass.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on December 08, 2021
//
import Foundation

struct GetModel: Codable {

	let userId: Int
	let id: Int
	let title: String
	let body: String

	private enum CodingKeys: String, CodingKey {
		case userId = "userId"
		case id = "id"
		case title = "title"
		case body = "body"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		userId = try values.decode(Int.self, forKey: .userId)
		id = try values.decode(Int.self, forKey: .id)
		title = try values.decode(String.self, forKey: .title)
		body = try values.decode(String.self, forKey: .body)
	}
}

struct arrayModel: Codable {

    let userId: Int
    let id: Int
    let title: String
    let body: String

    private enum CodingKeys: String, CodingKey {
        case userId = "userId"
        case id = "id"
        case title = "title"
        case body = "body"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        userId = try values.decode(Int.self, forKey: .userId)
        id = try values.decode(Int.self, forKey: .id)
        title = try values.decode(String.self, forKey: .title)
        body = try values.decode(String.self, forKey: .body)
    }

}
